import React,{Component} from 'react';
import ReactDOM from 'react-dom';

class Clock extends Component {
    constructor(props) {
        super(props);
        this.state = {date: new Date()}
    }

    render() {
        return (
            <div>
                <h1>定时器</h1>
                <h2>it is {this.state.date.toLocaleTimeString()}</h2>
            </div>
        );
    }
}

export default Clock