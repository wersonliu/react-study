import React, {Component} from 'react'
// import PropTypes from 'prop-types'
class ClickCounter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            count: props.initValue
        }
    }
    onClickIncre=()=>{
        this.onClickButton(true)
    };
    onClickDecr=()=>{
        this.onClickButton(false)
    };

    onClickButton = (isIncrement) => {
        const previousValue=this.state.count;
        const newValue=isIncrement?previousValue+1:previousValue-1;
        this.setState({
            count: newValue
        });
        this.props.onUpdate(newValue,previousValue)
    };

    render() {
        return (
            <div>
                <p>{this.props.caption}</p>
                <button onClick={this.onClickIncre.bind(this)}>+</button>
                <button onClick={this.onClickDecr.bind(this)}>-</button>
                <p>Count:{this.state.count}</p>
            </div>
        )
    }
}
ClickCounter.defaultProps={
    initValue:0
};
// ClickCounter.propTypes = {
//     caption: PropTypes.string.isRequired
// };

export default ClickCounter