import React, {Component} from 'react';

class Mfirst extends Component {
    render() {
        function handleClick(e) {
            e.preventDefault();
            alert('a被点击')
        }
        return (
            <div>
                <h1>菜鸟教程</h1>
                <h2>欢迎学习 React</h2>
                <a href="#" onClick={handleClick}>
                    点我
                </a>
            </div>
        )
    }
}

export default Mfirst