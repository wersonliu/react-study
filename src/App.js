import React from "react";
import "./App.css"
import ClickCounter from './component/Click-Counter'

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.initValues = [0, 10, 20];
        const initSum = this.initValues.reduce((a, b) => a + b, 0);
        this.state = {
            num: initSum
        }
    }

    onCounterUpdate = (newValue, previousValue) => {
        const valueChange = newValue - previousValue;
        this.setState({
            num: this.state.num + valueChange
        })
    };

    render() {
        return (
            <div>
                <ClickCounter caption='first' initValue={this.initValues[0]} onUpdate={this.onCounterUpdate}/>
                <ClickCounter caption='second' initValue={this.initValues[2]} onUpdate={this.onCounterUpdate}/>
                <ClickCounter caption='thread' initValue={this.initValues[1]} onUpdate={this.onCounterUpdate}/>
                <p>此时,三个计数器的值和为:{this.state.num}</p>
            </div>

        )
    }
}

// export default App;
